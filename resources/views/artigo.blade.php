
@extends('layouts.app')

@section('content')
<pagina tamanho="10">
    <painel>
    <h2 align='center'>{{$artigo->titulo}}</h2>
    <h4 align='center'>{{$artigo->descricao}}</h4>

        <div class="containerimg">
            <img class="img" src="{{'/'.$artigo->imagem}}" alt="...">
        </div>
        <br>
        <p><b>{{$artigo->titulo}}</b> -
            <small>{{$artigo->descricao}}</small>
        </p>
        <p>
            <small><b>Por: {{$artigo->user->name}}</b></small>
        </p>
        <hr>
    <p>
        {!!$artigo->conteudo!!}
    </p>

        <interacoes></interacoes>
        <div id="disqus_thread"></div>

        <p align='center'> <small>Por: {{$artigo->user->name}} - {{date('d/m/Y',strtotime($artigo->data))}}</small></p>
    </painel>
</pagina>

@endsection
<style>
    .img {
        width: 100%;
    }


</style>