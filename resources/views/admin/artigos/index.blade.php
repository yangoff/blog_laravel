@extends('layouts.app')

@section('content')
<pagina tamanho="12">
    @if($errors->all())

    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        @foreach($errors->all() as $key => $value)
        <p>{{$value}}</p>
        @endforeach
    </div>

    @endif
    <painel titulo="Lista de artigos">

        <migalhas v-bind:lista="{{$listaMigalhas}}"></migalhas>
        <tabela-lista v-bind:titulos="['#','Título','Descrição','autor','Data']" v-bind:itens="{{json_encode($listaArtigos)}}" criar="#criar" detalhe="/admin/artigos/" editar="/admin/artigos/" deletar="/admin/artigos/" token="{{csrf_token()}}" ordem="desc" ordemcol="0" modal="sim">
        </tabela-lista>
        <div align="center">
            {{$listaArtigos}}
        </div>
    </painel>
</pagina>

<!-- MODAL ADD -->
<modal nome="adicionar" titulo="Adicionar">
    <formulario id="formAdicionar" css="" action="{{route('artigos.store')}}" method="post"
                enctype="multipart/form-data" token="{{csrf_token()}}">

        <div class="form-group">
            <label for="titulo">Título</label>
            <input type="text" class="form-control" id="titulo" name="titulo" placeholder="Título" value="{{old('titulo')}}">
        </div>
        <div class="form-group">
            <label for="imagem">Imagem</label>
            <input class="form-control" type="file" name="imagem" id="imagem"
                   placeholder="O arquivo será redimensionado automaticamente" value="{{old('imagem')}}">
        </div>
        <div class="form-group">
            <label for="descricao">Descrição</label>
            <input type="text" class="form-control" id="descricao" name="descricao" placeholder="Descrição"
                   enctype="multipart/form-data" value="{{old('descricao')}}">
        </div>
        <div class="form-group">
            <label for="addConteudo">Conteudo</label>
            <ckeditor 
            id="addConteudo"
            name="conteudo"
            value="{{old('conteudo')}}" 
            :config="{
                toolbar: [
                    [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript' ]
                ],
                height: 300
            }">
            </ckeditor>
        </div>

        <div class="form-group">
            <label for="data">Data</label>
            <input type="date" class="form-control" id="data" name="data" value="{{old('data')}}">
        </div>


    </formulario>
    <span slot="botoes"><button form="formAdicionar" class="btn btn-info">Adicionar</button></span>

</modal>

<!-- MODAL EDIT -->
<modal nome="editar" titulo="Editar">
    <formulario id="formEditar" css="" v-bind:action="'/admin/artigos/' + $store.state.item.id" method="put" enctype="multipart/form-data" token="{{csrf_token()}}">
        <div class="form-group">
            <label for="titulo">Título</label>
            <input type="text" class="form-control" id="titulo" name="titulo" v-model="$store.state.item.titulo" placeholder="Título">
        </div>

        {{--        <div class="form-group">--}}
        {{--            <label for="imagem">Imagem</label>--}}
        {{--            <input type="image" class="form-control" id="imagem" name="imagem" >--}}
        {{--        </div>--}}
        <div class="container2">
            <div class="form-group">
                <label for="imagem">Imagem</label>
                <img class="img" :src="'../'+$store.state.item.imagem">
                <input class="form-control" type="file" name="imagem" id="imagem"
                       placeholder="O arquivo será redimensionado automaticamente">
            </div>
        </div>

        <div class="form-group">
            <label for="descricao">Descrição</label>
            <input type="text" class="form-control" id="descricao" v-model="$store.state.item.descricao" name="descricao" placeholder="Descrição">
        </div>
        <div class="form-group">
            <label for="editconteudo">Conteudo</label>
            <ckeditor
                    id="editconteudo"
                    name="conteudo"
                    v-model="$store.state.item.conteudo"
                    :config="{
                 toolbar: [[ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'Custom Button' ]],
                height: 300
            }"
            >
            </ckeditor>

            {{--        subistituir por:    //https://github.com/ankurk91/vue-trumbowyg--}}
        </div>

        <div class="form-group">
            <label for="data">Data</label>
            <input type="date" class="form-control" id="data" name="data" v-model="$store.state.item.data">
        </div>


    </formulario>
    <span slot="botoes"> <button form="formEditar" class="btn btn-info">Adicionar</button></span>
</modal>

<!-- MODAL DETAIL -->
<modal nome="detalhe" v-bind:titulo="$store.state.item.titulo">

    <p>@{{$store.state.item.descricao}}</p>
    <br>
    <p>@{{$store.state.item.conteudo}}</p>


</modal>
@endsection
<style>
    .img {
        width: 100% !important;
    }
</style>