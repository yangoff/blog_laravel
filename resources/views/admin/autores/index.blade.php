@extends('layouts.app')

@section('content')
<pagina tamanho="12">
    @if($errors->all())
    
    <div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  @foreach($errors->all() as $key => $value)
  <p>{{$value}}</p>
  @endforeach
</div>
    
    @endif
    <painel titulo="Lista de Autores">
    
        <migalhas v-bind:lista="{{$listaMigalhas}}"></migalhas>
        <tabela-lista v-bind:titulos="['#','Nome','Email']" v-bind:itens="{{json_encode($listaModelo)}}" criar="#criar" detalhe="/admin/autores/" editar="/admin/autores/" ordem="asc" ordemcol="2" modal="sim">
        </tabela-lista>
    <div align="center">
    {{$listaModelo}}
    </div>
    </painel>
</pagina>

<!-- MODAL ADD -->
<modal nome="adicionar" titulo="Adicionar">
    <formulario id="formAdicionar" css="" action="{{route('autores.store')}}" method="post" enctype="" token="{{csrf_token()}}">

        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Nome" value="{{old('name')}}">
        </div>
        <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="E-mail" value="{{old('email')}}">
        </div>
        <div class="form-group">
            <label for="autor">Autor</label>
            <select name="autor" id="autor" class="form-control">
                <option {{(old('autor') && old('autor') == 'N' ? 'selected' :'')}} value="N">Não</option>
                <option {{(old('autor') && old('autor') == 'S' ? 'selected' :'')}} {{(!old('autor') ? 'selected' :'')}} value="S">Sim</option>
            </select>
        </div>

        <div class="form-group">
            <label for="password">Senha</label>
            <input type="password" class="form-control" id="password" name="password" value="{{old('password')}}">
        </div>


    </formulario>
    <span slot="botoes"><button form="formAdicionar" class="btn btn-info">Adicionar</button></span>

</modal>

<!-- MODAL EDIT -->
<modal nome="editar" titulo="Editar">
    <formulario id="formEditar" css="" v-bind:action="'/admin/autores/' + $store.state.item.id" method="put" enctype="multipart/form-data" token="{{csrf_token()}}">

        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="name" name="name" v-model="$store.state.item.name" placeholder="Nome">
        </div>
        <div class="form-group">
            <label for="email">E-mail</label>
            <input type="text" class="form-control" id="email" v-model="$store.state.item.email" name="email" placeholder="E-mail">
        </div>
        <div class="form-group">
            <label for="autor">Autor</label>
            <select name="autor" v-model="$store.state.item.autor" id="autor"  class="form-control">
                <option  value="N">Não</option>
                <option  value="S">Sim</option>
            </select>
        </div>

        <div class="form-group">
            <label for="password">Senha</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>


    </formulario>
    <span slot="botoes"> <button form="formEditar" class="btn btn-info">Adicionar</button></span>
</modal>

<!-- MODAL DETAIL -->
<modal nome="detalhe" v-bind:titulo="$store.state.item.name">

    <p>@{{$store.state.item.name}}</p>
<br>
    <p>@{{$store.state.item.email}}</p>

</modal>
@endsection