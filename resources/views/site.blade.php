@extends('layouts.app')

@section('content')
<pagina tamanho="10">
    <painel titulo="Artigos">
        <p>
        <form class="form-inline text-center pull-right" action="{{route('site')}}" method="get">
            <input type="search" class="form-control" name="busca" placeholder="Buscar"
                   value="{{isset($busca) ? $busca: ''}}">
            <button class="btn btn-info">Buscar</button>

        </form>
        </p>
        <br><br><br>

        <div class="row">
    @foreach ($lista as $key => $value)
    <artigocard
            titulo="{{str_limit($value->titulo, 18, ' ...')}}"
            autor="{{$value->autor}}"
            data="{{$value->data}}"
            descricao="{{str_limit($value->descricao, 35, ' ...')}}"
            sm="6"
            md="4"
            link="{{route('artigo',[$value->id,str_slug($value->titulo)])}}"
            img="{{$value->imagem}}"
    >
    </artigocard>
  @endforeach
  </div>
  <div align="center">
            {{$lista}}
        </div>
    </painel>
</pagina>
@endsection