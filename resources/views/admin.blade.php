@extends('layouts.app')

@section('content')
<pagina tamanho="10">
    <painel titulo="Dashboard">

        <migalhas v-bind:lista="{{$listaMigalhas}}"></migalhas>
        <div class="row">
            @can('eAutor')
            <div class="col-md-4">
                <caixa v-bind:qtd="{{$count['artigos']}}" titulo="Artigos" url="{{route('artigos.index')}}" cor="orange" icon="ion ion-pie-graph"></caixa>
            </div>
            @endcan
            @can('eAdmin')
            <div class="col-md-4">
                <caixa v-bind:qtd="{{$count['usuarios']}}" titulo="Usuarios" url="{{route('usuarios.index')}}" cor="blue" icon="ion ion-person-stalker"></caixa>
            </div>
            <div class="col-md-4">
                <caixa v-bind:qtd="{{$count['autores']}}" titulo="Autores" url="{{route('autores.index')}}" cor="red" icon="ion ion-person"></caixa>
            </div>
            <div class="col-md-4">
                <caixa v-bind:qtd="{{$count['admin']}}" titulo="Admin" url="{{route('adm.index')}}" cor="green" icon="ion ion-person"></caixa>
            </div>
            @endcan


        </div>
    </painel>
</pagina>
@endsection