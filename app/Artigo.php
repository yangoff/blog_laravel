<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Artigo extends Model
{
    use SoftDeletes;

    protected $fillable = ['titulo', 'descricao', 'conteudo', 'data', 'imagem'];

    protected $dates=['deleted_at'];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public static function listaArtigos($paginate){
        $user = auth()->user();
        if ($user->admin == 'S') {
            return DB::table('artigos')
                ->join('users', 'users.id', '=', 'artigos.user_id')
                ->select('artigos.id', 'artigos.titulo', 'artigos.descricao', 'users.name', 'artigos.data')
                ->whereNull('artigos.deleted_at')
                ->orderBy('artigos.id', 'DESC')
                ->paginate($paginate);
        } else {
            return DB::table('artigos')
                ->join('users', 'users.id', '=', 'artigos.user_id')
                ->select('artigos.id', 'artigos.titulo', 'artigos.descricao', 'users.name', 'artigos.data')
                ->whereNull('artigos.deleted_at')
                ->where('artigos.user_id', '=', $user->id)
                ->orderBy('artigos.id', 'DESC')
                ->paginate($paginate);
        }

                        
    }

    public static function listaArtigosSite($paginate, $busca = null)
    {
        return $busca ? DB::table('artigos')
            ->join('users', 'users.id', '=', 'artigos.user_id')
            ->select('artigos.id', 'artigos.titulo', 'artigos.descricao', 'users.name as autor', 'artigos.data', 'artigos.imagem')
            ->whereNull('artigos.deleted_at')
            ->whereDate('data', '<=', date('Y-m-d'))
            ->where(function ($query) use ($busca) {
                $query->orWhere('titulo', 'like', '%' . $busca . '%')
                    ->orWhere('descricao', 'like', '%' . $busca . '%');
            })
            ->orderBy('data', 'DESC')
            ->paginate($paginate) : DB::table('artigos')
            ->join('users', 'users.id', '=', 'artigos.user_id')
            ->select('artigos.id', 'artigos.titulo', 'artigos.descricao', 'users.name as autor', 'artigos.data', 'artigos.imagem')
            ->whereNull('artigos.deleted_at')
            ->whereDate('data', '<=', date('Y-m-d'))
            ->orderBy('data', 'DESC')
            ->paginate($paginate);
     }
}
