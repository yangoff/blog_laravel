<?php

namespace App\Http\Controllers\Admin;

use App\Artigo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Image;
use Validator;

class ArtigosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $listaMigalhas= json_encode([
            ['titulo'=> 'Admin','url'=>route('admin')],
            ['titulo'=> 'Lista de Artigos','url'=>''],
        ]);
        // $listaArtigos= Artigo::select('id','titulo','descricao','user_id','data')->paginate(5);
        // foreach ($listaArtigos as $key => $value) {
        //     $value->user_id = \App\User::find($value->user_id)->name;
        // }


        $listaArtigos=Artigo::listaArtigos(5);

        return view('admin.artigos.index',compact('listaMigalhas','listaArtigos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validacao = Validator::make($data, [
            "titulo"=>"required",
            "descricao"=>"required",
            "conteudo"=>"required",
            "data" => "required",
            "imagem" => "mimetypes:image/jpg,image/jpeg"
        ]);
        if($validacao->fails()){
            return redirect()->back()->withErrors($validacao)->withInput();
        }
        $path = $request->file('imagem')->store('img/bg');
        Image::make('storage/' . $path)->resize(1200, 630)->save('storage/' . $path);
        $data['imagem'] = 'storage/' . $path;
        $user = auth()->user();
        $user->artigos()->create($data);

        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Artigo::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validacao = Validator::make($data, [
            "titulo"=>"required",
            "descricao"=>"required",
            "conteudo"=>"required",
            "data" => "required",
            "imagem" => "mimetypes:image/jpg,image/jpeg"
        ]);

        if($validacao->fails()){
            return redirect()->back()->withErrors($validacao)->withInput();
        }
        if (array_key_exists('imagem', $data)) {
            $path = $request->file('imagem')->store('img/bg');
            $data['imagem'] = 'storage/' . $path;
            Image::make('storage/' . $path)->resize(1200, 630)->save('storage/' . $path);
        }

        $user = auth()->user();
        $user->artigos()->find($id)->update($data);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Artigo::find($id)->delete();
        return redirect()->back();
    }
}
